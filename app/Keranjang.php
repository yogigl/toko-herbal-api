<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $fillabe = [
        'user_id'
    ];

    // keranjang belong to  many produk
    public function produk2()
    {
        return $this->belongsToMany(Produk::class, 'keranjang_produk');
    }

    // keranjang belong to user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public $timestamps = false;
}
