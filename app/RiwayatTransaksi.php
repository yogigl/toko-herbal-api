<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatTransaksi extends Model
{
    protected $fillable = [
        'transaksi_id', 'status_id', 'keterangan', 'diwaktu',
    ];

    // where transaksi
    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class);
    }

    // get info status
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public $timestamps = false;
}
