<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = [
        'nama', 'harga', 'deskripsi', 'url_gambar'
    ];

    // produk belong to many keranjang
    public function keranjangs()
    {
        return $this->belongsToMany(Keranjang::class, 'keranjang_produk');
    }

    // transakis of product
    public function transaksis()
    {
        return $this->belongsToMany(Transaksi::class, 'transaksi_produk');
    }

    public $timestamps = false;
}
