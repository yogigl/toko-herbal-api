<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransaksiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'ongkir' => $this->ongkir,
            'kurir' => $this->kurir,
            'promo' => $this->promo,
            'catatan' => $this->catatan,
            'alamat' => $this->alamat,
            'nohp' => $this->nohp,
            'dibuat' => $this->dibuat,
            "produk2" => $this->produk2,
            "user" => $this->user,
            "total" => $this->total()
        ];
    }
}
