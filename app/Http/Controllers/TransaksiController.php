<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Http\Resources\TransaksiResource;

class TransaksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        return TransaksiResource::collection(Transaksi::with(['produk2', 'user'])->paginate(27));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaksi = Transaksi::create([
            'user_id' => auth()->user()->id,
            'ongkir' => $request->ongkir ? $request->ongkir : 0,
            'kurir' => $request->kurir,
            'promo' => $request->promo ? $request->promo : 0,
            'catatan' => $request->catatan,
            'alamat' => $request->alamat,
            'nohp' => $request->nohp,
            'dibuat' => time(),
        ]);
        $transaksi->produk2()->sync($request->produk2);

        return new TransaksiResource($transaksi);
    }

    /**
     * Display the specified resource.
     *
     * @param  Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        if (auth()->user()->id != $transaksi->user_id || auth()->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        return new TransaksiResource(Transaksi::whereId($transaksi->id)->with(['produk2', 'total', 'user'])->first());
    }

}
