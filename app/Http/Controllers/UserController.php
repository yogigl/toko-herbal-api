<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        return UserResource::collection(User::with('keranjang')->paginate(27));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return new UserResource(User::whereId($request->user()->id)->with('keranjang')->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        if ($request->password !== null) {
            $request->merge(['password' => bcrypt($request->password)]);
        }

        $user->update(
            $request->only(['nama', 'email', 'password', 'nohp'])
        );

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        auth()->user()->delete();

        return response()->json(null, 204);
    }
}
