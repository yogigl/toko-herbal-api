<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keranjang;
use App\Http\Resources\KeranjangResource;

class KeranjangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        return KeranjangResource::collection(Keranjang::with('keranjang')->paginate(27));
    }

    /**
     * Display the specified resource.
     *
     * @param  Keranjang $keranjang
     * @return \Illuminate\Http\Response
     */
    public function show(Keranjang $keranjang)
    {
        if (auth()->user()->id != $keranjang->user_id || auth()->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }
        return new KeranjangResource(Keranjang::whereId($keranjang->id)->with('produk2')->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Keranjang $keranjang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keranjang $keranjang)
    {
        if (auth()->user()->id != $keranjang->user_id) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $keranjang->produk2()->sync($request->produk2);

        return new KeranjangResource(Keranjang::whereId($keranjang->id)->with('produk2')->first());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Keranjang $keranjang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keranjang $keranjang)
    {
        if (auth()->user()->id != $keranjang->user_id) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $keranjang->delete();

        return response()->json(null, 204);
    }
}
