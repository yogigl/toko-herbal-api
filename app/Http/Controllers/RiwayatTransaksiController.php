<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\RiwayatTransaksiResource;
use App\RiwayatTransaksi;

class RiwayatTransaksiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        return RiwayatTransaksiResource::collection(RiwayatTransaksi::with(['transaksi', 'status'])->paginate(27));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $riwayatTransaksi = RiwayatTransaksi::create([
            'transaksi_id' => $request->transaksi_id,
            'status_id' => $request->status_id,
            'keterangan' => $request->keterangan,
            'diwaktu' => time(),
        ]);

        return new RiwayatTransaksiResource(RiwayatTransaksi::whereId($riwayatTransaksi->id)->with(['transaksi', 'status'])->first());
    }

    /**
     * Display the specified resource.
     *
     * @param  RiwayatTransaksi  $riwayatTransaksi
     * @return \Illuminate\Http\Response
     */
    public function show(RiwayatTransaksi $riwayatTransaksi)
    {
        if (auth()->user()->id != $riwayatTransaksi->transaksi()->user_id || auth()->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        return new RiwayatTransaksiResource(RiwayatTransaksi::whereId($riwayatTransaksi->id)->with(['transaksi', 'status'])->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  RiwayatTransaksi  $riwayatTransaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RiwayatTransaksi $riwayatTransaksi)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $riwayatTransaksi->update($request->only('keterangan'));

        return new RiwayatTransaksiResource($riwayatTransaksi);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  RiwayatTransaksi  $riwayatTransaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(RiwayatTransaksi $riwayatTransaksi)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $riwayatTransaksi->delete();

        return response()->json(null, 204);
    }
}
