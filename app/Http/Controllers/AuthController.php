<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Keranjang;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $user = User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'nohp' => $request->nohp,
            'password' => bcrypt($request->password),
            'ini_pelanggan' => $request->ini_pelanggan,
            'dibuat' => time(),
        ]);

        $user->keranjang()->save(new Keranjang);

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(
                ['error' => 'Tidak memiliki akses'],
                401
            );
        }

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token)
    {
        return response()->json(
            [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expire_in' => auth()->factory()->getTTL() * 60
            ]
        );
    }
}
