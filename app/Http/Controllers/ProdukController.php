<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use App\Http\Resources\ProdukResource;

class ProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProdukResource::collection(Produk::paginate(27));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $produk = Produk::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'url_gambar' => $request->url_gambar
        ]);

        return new ProdukResource($produk);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk2)
    {
        return new ProdukResource($produk2);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk2)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $produk2->update($request->all());

        return new ProdukResource($produk2);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk2)
    {
        if ($request->user()->ini_pelanggan) {
            return response()->json(['error' => 'Tidak memiliki akses'], 403);
        }

        $produk2->delete();

        return response()->json(null, 204);
    }
}
