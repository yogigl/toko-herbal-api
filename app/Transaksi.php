<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = [
        'user_id', 'dibuat', 'ongkir', 'kurir', 'promo', 'catatan', 'alamat', 'nohp'
    ];

    // who buy
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // produk2 in this transaksi
    public function produk2()
    {
        return $this->belongsToMany(Produk::class, 'transaksi_produk');
    }

    public function total()
    {
        $total = 0;
        $produk2 = $this->produk2;
        if (!$produk2->isEmpty()) {
            foreach ($produk2 as $produk) {
                $total += (int)$produk->harga;
            }
            $total += (int)$this->ongkir;
            $total += (int)$this->promo;
        }
        return $total;
    }

    public $timestamps = false;
}
