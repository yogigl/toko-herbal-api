<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [
        'nama'
    ];

    // riwayat transaksi in transaksi
    public function riwayats()
    {
        return $this->hasMany(RiwayatTransaksi::class);
    }

    public $timestamps = false;
}
