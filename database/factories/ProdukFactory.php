<?php

use Faker\Generator as Faker;

$factory->define(App\Produk::class, function (Faker $faker) {
    return [
        'nama' => $faker->unique()->word,
        'harga' => $faker->numberBetween($min = 30000, $max = 500000),
        'deskripsi' => $faker->sentence,
        'url_gambar' => 'https://placehold.jp/200x300.png'
    ];
});
