<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 13)->create()->each(function ($u) {
            $u->keranjang()->save(new App\Keranjang);
        });
    }
}
