<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusName = ['cek', 'menunggu_pembayaran', 'sudah_dibayar', 'menunggu_pengiriman', 'sedang_dkirim', 'sudah_diterima', 'pengembalian'];
        foreach ($statusName as $key => $value) {
            App\Status::create([
                'nama' => $value
            ]);
        }
    }
}
