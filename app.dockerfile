FROM php:fpm-alpine

RUN set -ex \
    && apk --no-cache add \
    postgresql-dev \
    libzip-dev \
    zip \
    unzip \
    git

RUN docker-php-ext-configure zip --with-libzip

RUN docker-php-ext-install pdo pdo_pgsql

RUN curl --silent --show-error https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer