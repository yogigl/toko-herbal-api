<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::get('saya', 'UserController@show');
Route::patch('saya', 'UserController@update');
Route::delete('saya', 'UserController@destroy');

Route::get('pengguna2', 'UserController@index');
Route::post('daftar', 'AuthController@register');
Route::post('masuk', 'AuthController@login');

Route::apiResource('produk2', 'ProdukController');
Route::apiResource('keranjang', 'KeranjangController')->except('store');
Route::apiResource('transaksi', 'TransaksiController')->only('index', 'store', 'show');
Route::apiResource('riwayat-transaksi', 'RiwayatTransaksiController')->parameters([
    'riwayat-transaksi' => 'riwayatTransaksi'
]);

